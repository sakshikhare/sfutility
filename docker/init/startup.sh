. /myntra/$APP/bin/setenv.sh
mkdir -p /myntra/$APP/logs
if [ -d "$CATALINA_HOME" ]; then
        cd $CATALINA_HOME/bin
        ./startup.sh "$@"
else
        echo "$CATALINA_HOME does not exist"
fi