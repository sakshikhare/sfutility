package api;

import com.google.gson.Gson;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

@Path("/test")
public class Service {
    String host ="";
    String hostname = "d7" + host + ".myntra.com";


    @GET
    @Path("/verify")
    @Produces(MediaType.TEXT_PLAIN)
    public Response verifyRESTService(InputStream incomingData) {
        String result = "CrunchifyRESTService Successfully started..";
        return Response.status(200).entity(result).build();
    }

    @POST
    @Path("/getFirst")
    @Consumes("application/json")
    @Produces("application/json")
    public Response getFirst(@HeaderParam("database") String database, @HeaderParam("collection") String collection, @HeaderParam("host") String host) {
        Document myDoc = new Document();
        if(host.equalsIgnoreCase("localhost")){
            hostname ="localhost";

        }
        try {
            MongoClient mongoClient = new MongoClient(hostname, 27017);
            MongoDatabase db = mongoClient.getDatabase(database);
            MongoCollection<Document> table = db.getCollection(collection);
            myDoc = table.find().first();
        } catch (Exception e) {
            System.out.println(e);
        }
        return Response.status(200).entity(myDoc.toJson()).build();

    }

    @POST
    @Path("/getAll")
    @Consumes("application/json")
    @Produces("application/json")

    public Response getAll(@HeaderParam("database") String database, @HeaderParam("collection") String collection, @HeaderParam("host") String host) {
        List<Document> documents = new ArrayList<Document>();
        Gson gson = new Gson();
        if(host.equalsIgnoreCase("localhost")){
            hostname ="localhost";

        }
        try {
            MongoClient mongoClient = new MongoClient(hostname, 27017);
            MongoDatabase db = mongoClient.getDatabase(database);
            MongoCollection<Document> table = db.getCollection(collection);
            documents = table.find().into(new ArrayList<Document>());
            } catch (Exception e) {
            System.out.println(e);
        }
        return Response.status(200).entity(gson.toJson(documents)).build();

    }
}
